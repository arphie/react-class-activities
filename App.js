/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Modal
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Two from './screens/activityTwo'
import Three from './screens/activityThree'
import Foura from './screens/activityFoura'
import Fourb from './screens/activityFourb'
import Five from './screens/activityFive'
import Six from './screens/activitySix'
import Seven from './screens/activitySeven'
import Eight from './screens/activityEight'
import Nine from './screens/activityNine'
import Ten from './screens/activityTen'
import Eleven from './screens/activityEleven'
import WebViewer from './screens/activityWebView'
import Dmodal from './screens/activityModal'
import MyActivityIndicator from './screens/activityActivityIndicator'
import MyPicker from './screens/activityPicker'
import MyStatusBar from './screens/activityStatusBar'
import MySwitch from './screens/activitySwitch'
import MyText from './screens/activityText'
import MyAlert from './screens/activityAlert'



const App: () => Node = () => {

  return (
    // <View>
      <MyAlert />
    // </View>
  );

};

export default App;
