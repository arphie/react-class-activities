import React, { useState } from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Button
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Ten = () => {

  const [count, setCount] = useState(0);

  const countHandler = () => {
    let RandomNumber = Math.floor(Math.random() * 100) + 1;
    setCount(RandomNumber);
  }

  return (
    <View>
      <View style={styles.buttonContainer}>
          <Button title='Generate Random Number from 1 - 100' onPress={countHandler} />
      </View>
      <View style={styles.counterDesign}>
        <Text style={styles.textCounter}>New Random number: { count }</Text>
      </View>
      
    </View>
  );
}

const styles = StyleSheet.create({
  counterDesign: {
    marginTop: 14
  },
  textCounter:{
    color: 'red'
  }
});


export default Ten;
