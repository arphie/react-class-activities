import React, { useState } from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Button
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Two = () => {

  const [count, setCount] = useState(0);

  const countHandler = () => {
    let newCount = count + 1;
    setCount(newCount);
  }

  return (
    <View>
      <View style={styles.buttonContainer}>
          <Button title='click me' onPress={countHandler} />
      </View>
      <View style={styles.counterDesign}>
        <Text style={styles.textCounter}>You clicked { count } times</Text>
      </View>
      
    </View>
  );
}

const styles = StyleSheet.create({
  counterDesign: {
    marginTop: 14,
  },
  textCounter:{
    color: 'red'
  },
  mainContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});


export default Two;
