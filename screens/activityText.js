import React, { useState } from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Button
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const MyText = () => {

  const [count, setCount] = useState(0);

  const countHandler = () => {
    let RandomNumber = Math.floor(Math.random() * 100) + 1;
    setCount(RandomNumber);
  }

  return (
    <View>
      <Text>This is the Text Activity</Text>
      
    </View>
  );
}

const styles = StyleSheet.create({
  counterDesign: {
    marginTop: 14
  },
  textCounter:{
    color: 'red'
  }
});


export default MyText;
