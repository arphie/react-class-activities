import React, { useState } from 'react';
import type {Node} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';


const Eight = () => {

  return (
    <View>
      <View style={styles.imageHolder}>
        <Image style={styles.actualImage} source={require('../assets/bark_side.jpg')} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  counterDesign: {
    marginTop: 14,
    flex: 1,
  },
  actualImage: {
    width: 400
  }
});


export default Eight;
