import React, { useState } from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Button,
  FlatList
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Seven = () => {
    const [humans, setHumans] = useState([
        {name: 'human 1', id: 1},
        {name: 'human 2', id: 2},
        {name: 'human 3', id: 3},
        {name: 'human 4', id: 4},
        {name: 'human 5', id: 5},
        {name: 'human 6', id: 6},
        {name: 'human 7', id: 7},
        {name: 'human 8', id: 8},
        {name: 'human 9', id: 9},
        {name: 'human 10', id: 10},
        {name: 'human 11', id: 11},
        {name: 'human 12', id: 12},
    ]);

  return (
    <View style={styles.mainComponent}>
      <ScrollView>
        { humans.map((item) => {
            return (
              <View key={item.id}>
                <Text style={styles.peopleList}>{item.name}</Text>
              </View>
            )
          }) }
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  mainComponent: {
    flex: 1,
    // justifyContent: "center",
    // alignItems: "center",
    paddingTop: 40,
    paddingHorizontal: 20
    // paddingTop: 50,
  },
  peopleList: {
      color: 'black',
      backgroundColor: 'pink',
      padding: 20,
      margin: 5
  }
});


export default Seven;
