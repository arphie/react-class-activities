import React, { useState, useEffect, } from 'react';
import type {Node} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  SafeAreaView,
  Flatlist,
  ActivityIndicator
} from 'react-native';

const sampleUrl = "http://jsonplaceholder.typicode.com/posts/1";

const Nine = () => {

  const [data, setData] = useState([]);

  useEffect(
    () => {
      fetch(sampleUrl,
      {
        method: 'GET'
      })
      .then((response) => response.json())
      .then(
        (json) => setData(json)
      ).catch(
        function(error) {
          console.log('There has been a problem with your fetch operation: ' + error.message);
          // ADD THIS THROW error
          throw error;
        }
      )
    }
  );

  return (
    <View>
      <View style={styles.imageHolder}>
        <Text>{data.body}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  counterDesign: {
    marginTop: 14,
    flex: 1,
  },
  actualImage: {
    width: 400
  }
});


export default Nine;
