import React, { useState } from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Button,
  FlatList
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Five = () => {
    const [humans, setHumans] = useState([
        {name: 'human 1', id: 1},
        {name: 'human 2', id: 2},
        {name: 'human 3', id: 3},
        {name: 'human 4', id: 4},
        {name: 'human 5', id: 5},
        {name: 'human 6', id: 6},
        {name: 'human 7', id: 7},
    ]);

  return (
    <View style={styles.mainComponent}>
      <FlatList
        data={humans}
        renderItem={({item}) => (
            <Text style={styles.peopleList}>{item.name}</Text>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  mainComponent: {
    justifyContent: "center",
    alignItems: "center",
    // paddingTop: 50,
  },
  peopleList: {
      color: 'black',
      backgroundColor: 'pink',
      padding: 20,
      margin: 5
  }
});


export default Five;
