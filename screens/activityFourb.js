import React, { useState } from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Button
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Fourb = () => {

  return (
    <View style={styles.mainComponent}>
      <Text style={styles.boxOne}>box 1</Text>
      <Text style={styles.boxFour}>box 2</Text>
      <Text style={styles.boxThree}>box 3</Text>
      
    </View>
  );
}

const styles = StyleSheet.create({
  mainComponent: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    backgroundColor: 'grey',
    height: 600
  },
  boxOne: {
      backgroundColor: 'pink',
      padding: 10,
      width: 100,
      height: 100,
  },
  boxFour: {
      backgroundColor: 'coral',
      padding: 10,
      width: 100,
      height: 100,
  },
  boxThree: {
      backgroundColor: 'orange',
      padding: 10,
      width: 100,
      height: 100,
  },
});


export default Fourb;
