import React, { useState } from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Button
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Foura = () => {

  return (
    <View style={styles.mainComponent}>
      <Text style={styles.boxOne}>box 1</Text>
      <Text style={styles.boxFour}>box 2</Text>
      <Text style={styles.boxThree}>box 3</Text>
      
    </View>
  );
}

const styles = StyleSheet.create({
  mainComponent: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 50,
  },
  boxOne: {
      backgroundColor: 'pink',
      padding: 10,
  },
  boxFour: {
      backgroundColor: 'coral',
      padding: 10,
  },
  boxThree: {
      backgroundColor: 'orange',
      padding: 10,
  },
});


export default Foura;
