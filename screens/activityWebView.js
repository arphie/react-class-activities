import React, { useState } from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Button
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { WebView } from 'react-native-webview';

const WebViewer = () => {

  const [count, setCount] = useState(0);

  const countHandler = () => {
    let RandomNumber = Math.floor(Math.random() * 100) + 1;
    setCount(RandomNumber);
  }

  return <WebView source={{ uri: 'https://reactnative.dev/' }} />;
}

const styles = StyleSheet.create({
  counterDesign: {
    marginTop: 14
  },
  textCounter:{
    color: 'red'
  }
});


export default WebViewer;
