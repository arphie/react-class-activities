import React, { useState } from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Button,
  FlatList,
  TextInput
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Six = () => {
  const [human, setHuman] = useState('human');
  const [age, setAge] = useState(30);

  const changeName = (val) => {
    setHuman(val);
  }

  return (
    <View style={styles.mainComponent}>
      <Text>Enter Name: </Text>
      <TextInput
        style={styles.nameInput}
        placeholder='John Peter Doe'
        onChangeText={changeName}
      />

      <Text>Hi! i am {human} and i am {age} years old</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  mainComponent: {
    justifyContent: "center",
    alignItems: "center",
    // paddingTop: 50,
    flex: 1
  },
  nameInput: {
    borderWidth: 1,
    borderColor: '#777',
    padding: 8,
    margin: 8,
    width: 300
  }
});


export default Six;
